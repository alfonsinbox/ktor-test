package demo

import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.route

fun Route.userRoute() {
    route("users") {
        authenticate {
            get {
                call.respond("Me")
            }
        }
        get("{id}") {
            call.respond("Users here!")
        }
    }
}
