package demo

import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.route

fun Route.loginRoute() {
    route("login") {
        get {
            val token = JwtConfig.makeToken(User(1, "", listOf()))
            call.respond(token)
        }
    }
}