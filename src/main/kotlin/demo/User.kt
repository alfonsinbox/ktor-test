package demo

import io.ktor.auth.*

// From https://github.com/AndreasVolkmann/ktor-auth-jwt-sample
data class User(
        val id: Int,
        val name: String,
        val countries: List<String>
) : Principal
