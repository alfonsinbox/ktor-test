package demo

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.jwt.jwt
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson
import io.ktor.routing.routing

fun Application.myModule() {
    install(CallLogging) {
        
    }
    install(ContentNegotiation) {
        jackson()
    }
    install(Authentication) {
        jwt {
            realm = "ktor.io"
            verifier(JwtConfig.verifier)
            validate { User(5, "Alfons", listOf("Mitt land!")) }
        }
    }
    routing {
        loginRoute()
        userRoute()
    }
}
