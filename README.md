# A test web application using kotlin with ktor

This is nothing more than a test. The things tested here so far are the ktor framwork and the CI/CD functionality of GitLab for deploying a Docker image to Google Cloud Registry.

## GitLab CI/CD Setup
- Go to the Google Cloud Console and create a Service Account with Storage Admin as Role. Make sure to download the key file in json format.
- Set a GitLab variable named `GCP_SERVICE_ACCOUNT_PRODUCTION_KEY` to the contents of the json file.
- Set a GitLab variable named `GCP_SERVICE_ACCOUNT_PRODUCTION_ADDRESS` to the address of the Service Account.

